<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function test(){
      $angka_1 = 1 ;
      $angka_2 = 5 ;

      $jumlah = $angka_1 + $angka_2 ;

      echo $jumlah;
    }

    public function index(){
      $semua_kategori = Category::all();

      return view('category.index' , compact('semua_kategori'));
    }

    public function create(){
      return view('category.create');
    }

    public function store(Request $request){
      $kategori = new Category;
      $kategori->nama_kategori = $request->nama_kategori;
      $kategori->save();

      return redirect('/kategori');
    }

    public function edit($id_kategori){
      $kategori = Category::find($id_kategori);

      return view('category.edit', compact('kategori'));
    }

    public function update(Request $request, $id_kategori) 
    {
      // dd($request->all());
      $kategori_to_update = Category::find($id_kategori);
      $kategori_to_update->nama_kategori = $request["nama_kategori"];
      
      $kategori_to_update->save();

      return redirect('/kategori');

    }

    public function destroy($id_kategori)
    {
      Category::destroy($id_kategori);

      return redirect('/kategori');
    }

    public function show($id_kategori)
    {
      $kategori = Category::find($id_kategori);

      return view('category.show', compact('kategori'));
    }










    // public function __construct() {
    //     return $this->middleware('auth');
    // }
    //
    // public function json(){
    //     return Datatables::of(Category::all())->make(true);
    // }
    //
    // public function index()
    // {
    //     $categories = Category::all(); // -> SELECT * FROM categories;
    //     //dd($categories);
    //     return view('categories.index', compact('categories'));
    // }
    //
    // public function create() {
    //     return view('categories.create');
    // }
    //
    // public function store(Request $request)
    // {
    //     //disimpen datanya ke database
    //     // dd($request->all());
    //     $this->validate($request, [
    //         "name" => 'required|max:255'
    //     ]);
    //
    //     $category = new Category;
    //     $category->name = $request["name"];
    //     $category->save();
    //
    //     return redirect('/admin/categories')->with(['messages' => 'data berhasil ditambahkan!']);
    // }
    //
    // public function show($id)
    // {
    //     $category = Category::find($id);
    //     return view('categories.show', compact('category'));
    // }
    //
    // public function edit($id)
    // {
    //     $category = Category::find($id);
    //     return view('categories.edit', compact('category'));
    // }
    //
    // public function update(Request $request, $id) {
    //     // dd($request->all());
    //
    //     $category = Category::find($id);
    //     $category->name = $request["name"];
    //     $category->save();
    //
    //     return redirect('/admin/categories');
    // }
    //
    // public function destroy($id)
    // {
    //     Category::destroy($id);
    //
    //     return redirect('/admin/categories');
    // }
}
