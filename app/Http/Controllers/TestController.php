<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class TestController extends Controller
{
    public function test(){
      $categories = Category::all();

      return view('test.index' , compact('categories'));
    }
}
