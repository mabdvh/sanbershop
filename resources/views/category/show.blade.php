@extends('admin-newest.layouts.app')

@section('title', 'Category - Edit')

@section('content')

  <h3> Halaman Edit Kategori id = {{ $kategori->id }} </h3>
  <table class="table">
    <tr>
      <th>Nama Kategori</th>
      <td> {{ $kategori->nama_kategori }} </td>
    </tr>
  </table>
  
@endsection