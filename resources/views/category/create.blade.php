@extends('admin-newest.layouts.app')

@section('title' , 'Category - Create')

@section('content')
  <form class="" action="{{ url('/kategori/create') }}" method="post">
    <input type="text" class="form-control" name="nama_kategori" placeholder="isi nama kategori">
    {{ csrf_field() }}
    <input type="submit" class="btn btn-success" name="" value="Submit">
  </form>
@endsection
